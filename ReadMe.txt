GameControllerWOI ReadMe
------------------------

To integrate the module in an existing cocos2d-x project:
---------------------------------------------------------
1. Include the java classes provided in the 'Source' folder in your project's android source

2. Do the following in your game's main activity:
   |- Initiate the GameControllerWOI class
   |- Implement the InputDeviceListener protocol and register with InputManagerCompat
   `- Implement the following methods and call corresponding GameControllerWOI methods of similar name:
	|- public boolean dispatchGenericMotionEvent(MotionEvent ev)
	|- public boolean dispatchKeyEvent(KeyEvent event)
	|- public void onInputDeviceAdded(int deviceId)
	|- public void onInputDeviceChanged(int deviceId)
	`- public void onInputDeviceRemoved(int deviceId)

3. Include the 'Raw/Handset' folder and all its files among your game's resources


A brief on how the module works:
--------------------------------
1. Upon initialization, the GameControllerWOI class parses the HandsetInfo.xml to get a list of "recognized" handsets/joysticks.
2. When a device is added, its name is searched in the list of "recognized" handsets/joysticks.
3. If the device is recognized, a corresponding XML file describing its key values is loaded, and the values are mapped to cocos2d-x game controller keys. 
   If the device is not recognized, an XML file containing default Android key values is loaded and mapped.
4. When a key event is received, the received key code is translated into cocos2d-x game controller keys using the map created in step 3, and then dispatched to C++ using the cocos2d-x GameControllerAdapter interface.
