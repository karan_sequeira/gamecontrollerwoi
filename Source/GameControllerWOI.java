package aero.woi.startupproject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.GameControllerAdapter;
import org.cocos2dx.lib.GameControllerDelegate;

import android.app.Activity;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;

public class GameControllerWOI {
	
	private static final String TAG = "WOI-DEBUG";
	private final int DEFAULT_DEVICE_ID = 0; 

	private Activity startUpActivity = null;
	private static String resourcePath = "";

	// This HashMap, maps a handset's name to its key values XML file
	private HashMap<String, String> handsetNameIdMap = null;
	private String connectedHandsetId = "";
	private String connectedDeviceName = "";
	private int connectedDeviceId = -1;
	
	private SparseIntArray controllerKeyMap = null;
	private SparseIntArray externalKeyMap = null;
	
	public GameControllerWOI(Activity activity) {
		startUpActivity = activity;
		controllerKeyMap = new SparseIntArray(16);			// at the time of writing this, there were 16 entries in each handset's XML file
		externalKeyMap = new SparseIntArray();
		
		getCustomHandsetNames();
		gatherControllers();
	}

	/*
	 * This function parses the Handset_info.xml file to generate a map of handset ids, for which we want to define custom key values
	 */
	@SuppressWarnings("unchecked")
	private void getCustomHandsetNames() {
		handsetNameIdMap = new HashMap<String, String>();
		
		InputStream is = getInputStreamForFile(GameControllerWOI.resourcePath + "/Raw/Handset/Handset_Info.xml");		//assetManager.open(GameControllerWOI.resourcePath + "/Raw/Handset/Handset_Info.xml");
		if(is == null)
		{
			Log.e(TAG, "Could not find Handset_Info.xml");
			return;
		}
		
		XMLPropertyListConfiguration parser = new XMLPropertyListConfiguration(is);
		Set<String> handsetNames = parser.getKeys();
		for (Iterator<String> iter = handsetNames.iterator(); iter.hasNext();) {
			String handsetName = iter.next();
			HashMap<String, Object> handset = (HashMap<String, Object>) parser.getConfigurationObject(handsetName);
			String handsetId = (String)handset.get("HandsetID");
			handsetNameIdMap.put(handsetName, handsetId);
		}
	}

	/*
	 * This function will query the connected devices, compare with the 'controllerNameIdMap' and accordingly load key values.
	 * */
	private void gatherControllers() {
		// loop through all connected devices
		int[] deviceIds = InputDevice.getDeviceIds();
		for (int deviceId : deviceIds) {
			InputDevice device = InputDevice.getDevice(deviceId);
			int deviceSource = device.getSources();

			// consider only gamepads and joysticks
			if (((deviceSource & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)
					|| ((deviceSource & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) {
				String deviceName = device.getName();
				
				// load custom values only if this device is listed in Handset_Info.xml
				if(handsetNameIdMap.containsKey(deviceName)) {
					if(loadKeyValuesForHandset(handsetNameIdMap.get(deviceName))) {
						setConnectedHandset(handsetNameIdMap.get(deviceName), deviceName, deviceId);
						return;
					}
				}
			}
		}
		
		// if connected device is not listed in Handset_Info.xml, load default values
		if(loadKeyValuesForHandset("default")) {
			setConnectedHandset("default", "default", DEFAULT_DEVICE_ID);
		}
	}

	/*
	 * This function will parse the XML file detailing the custom key codes for the device described by parameter handsetId.
	 * The values found as a result of the parsing will be appropriately mapped to GameController keys.
	 * */
	@SuppressWarnings("unchecked")
	private boolean loadKeyValuesForHandset(String handsetId) {
		// only load values if a different handset has been connected
		if(handsetId == connectedHandsetId) {
			return true;
		}
		
		InputStream is = getInputStreamForFile(GameControllerWOI.resourcePath + "/Raw/Handset/" + handsetId + ".xml");
		if(is == null)
		{
			Log.e(TAG, "Could not find key values file:" + handsetId + ".xml");
			return false;
		}
		
		// parse the XML
		XMLPropertyListConfiguration parser = new XMLPropertyListConfiguration(is);
		// fetch the map containing the custom key names and their values
		HashMap<String, Object> keyValues = (HashMap<String, Object>) parser.getConfigurationObject("KeyValues");
		
		// first flush the maps to clear any previously mapped keys
		controllerKeyMap.clear();
		externalKeyMap.clear();
		
		// map WOI keys to respective GameController keys
		controllerKeyMap.put((Integer)keyValues.get("kTypeKeyboardBackClicked"), KeyEvent.KEYCODE_BACK);
		controllerKeyMap.put((Integer)keyValues.get("kTypeKeyboardStartClicked"), GameControllerDelegate.BUTTON_START);
		controllerKeyMap.put((Integer)keyValues.get("kTypeLeftDpadClicked"), GameControllerDelegate.BUTTON_DPAD_LEFT);
		controllerKeyMap.put((Integer)keyValues.get("kTypeRightDpadClicked"), GameControllerDelegate.BUTTON_DPAD_RIGHT);
		controllerKeyMap.put((Integer)keyValues.get("kTypeUpDpadClicked"), GameControllerDelegate.BUTTON_DPAD_UP);
		controllerKeyMap.put((Integer)keyValues.get("kTypeDownDpadClicked"), GameControllerDelegate.BUTTON_DPAD_DOWN);
		controllerKeyMap.put((Integer)keyValues.get("kTypeAClicked"), GameControllerDelegate.BUTTON_A);
		controllerKeyMap.put((Integer)keyValues.get("kTypeBClicked"), GameControllerDelegate.BUTTON_B);
		controllerKeyMap.put((Integer)keyValues.get("kTypeXClicked"), GameControllerDelegate.BUTTON_X);
		controllerKeyMap.put((Integer)keyValues.get("kTypeYClicked"), GameControllerDelegate.BUTTON_Y);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyBackClicked"), KeyEvent.KEYCODE_BACK);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyStartClicked"), GameControllerDelegate.BUTTON_START);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyAClicked"), GameControllerDelegate.BUTTON_A);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyBClicked"), GameControllerDelegate.BUTTON_B);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyXClicked"), GameControllerDelegate.BUTTON_X);
		controllerKeyMap.put((Integer)keyValues.get("kTypeJoyYClicked"), GameControllerDelegate.BUTTON_Y);
		
		// map all back keys to Android Back
		externalKeyMap.append(KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_BACK);
		
		return true;
	}
	
	/*
	 * This method stores required information for the connected handset and informs the game that a handset has been connected.
	 * Call this method after the key values have been mapped successfully.
	 * */
	private void setConnectedHandset(String handsetId, String deviceName, int deviceId) {
		connectedHandsetId = handsetId;
		connectedDeviceName = deviceName;
		connectedDeviceId = deviceId;
		GameControllerAdapter.onConnected(deviceName, deviceId);
	}

	public boolean dispatchGenericMotionEvent(MotionEvent event) {
		boolean handled = false;
		
		int eventSource = event.getSource();
		if (((eventSource & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)
				|| ((eventSource & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) {
			if (event.getAction() == MotionEvent.ACTION_MOVE) {
				int deviceId = event.getDeviceId();
				
				String deviceName = "";
				// for some devices, event.getDevice() returns null
				try {
					deviceName = event.getDevice().getName();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				
				float leftThumbstickX = event.getAxisValue(MotionEvent.AXIS_X);
				if(leftThumbstickX <= -0.75) {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_LEFT, true, 1.0f, false);
				}
				else if(leftThumbstickX >= 0.75) {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_RIGHT, true, 1.0f, false);
				}
				else {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_LEFT, false, 1.0f, false);
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_RIGHT, false, 1.0f, false);
				}
				
				float leftThumbstickY = event.getAxisValue(MotionEvent.AXIS_Y);
				if(leftThumbstickY <= -0.75) {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_UP, true, 1.0f, false);
				}
				else if(leftThumbstickY >= 0.75) {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_DOWN, true, 1.0f, false);
				}
				else {
					handled = true;
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_UP, false, 1.0f, false);
					GameControllerAdapter.onButtonEvent(deviceName, deviceId, GameControllerDelegate.BUTTON_DPAD_DOWN, false, 1.0f, false);
				}
			}
		}
		
		return handled;
	}

	public boolean dispatchKeyEvent(KeyEvent event) {
		boolean handled = false;

		int eventSource = event.getSource();
		int deviceId = event.getDeviceId();
		
		String deviceName = "";
		// for some devices, event.getDevice() returns null
		try {
			deviceName = event.getDevice().getName();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		int keyCode = event.getKeyCode();
		int controllerKey = controllerKeyMap.get(keyCode);
		
/*		Log.d(TAG, "KEY - " + keyCode + 
				(event.getAction() == KeyEvent.ACTION_DOWN ? " PRESSED" : " RELEASED") +
				" GAMEPAD/JOYSTICK=" + ((((eventSource & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)	|| ((eventSource & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) ? "YES" : "NO") +
				" ID=" + deviceId);*/
		
		// controller events will only be sourced from GAMEPAD or JOYSTICK devices
		if (((eventSource & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)
				|| ((eventSource & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK)) {
			if (controllerKey == 0) {
				// This return statement means that keys that we have not defined, will not be dispatched to the game!
				return false;
			}
		}
		else {
			// External keys such as the 'Android back' key, will not be sourced from GAMEPAD or JOYSTICK devices hence this 'else' block has been added.
			int externalKey = externalKeyMap.get(keyCode);
			if(externalKey != 0) {
				controllerKey = externalKey;
			}
			else {
				return false;
			}
		}

		int action = event.getAction();
		if (action == KeyEvent.ACTION_DOWN) {
			handled = true;
			GameControllerAdapter.onButtonEvent(deviceName, deviceId, controllerKey, true, 1.0f, false);
		} else if (action == KeyEvent.ACTION_UP) {
			handled = true;
			GameControllerAdapter.onButtonEvent(deviceName, deviceId, controllerKey, false, 0.0f, false);
		}
		
		return handled;
	}

	public void onInputDeviceAdded(int deviceId) {
		Log.d(TAG, "GameControllerWOI.onInputDeviceAdded");
		gatherControllers();
	}

	public void onInputDeviceChanged(int deviceId) {
		Log.d(TAG, "GameControllerWOI.onInputDeviceChanged");
	}

	public void onInputDeviceRemoved(int deviceId) {
		Log.d(TAG, "GameControllerWOI.onInputDeviceRemoved");
		if(deviceId == connectedDeviceId) {
			GameControllerAdapter.onDisconnected(connectedDeviceName, connectedDeviceId);
		}
	}

	public static void setResourcePath(String resourcePath) {
		GameControllerWOI.resourcePath = resourcePath;
	}
	
	private void dumpMappedKeys() {
		Log.d(TAG, "---------------------------------------------");
		Log.d(TAG, "Dumping all mapped keys:");
		for (int i = 0; i < controllerKeyMap.size(); ++i) {
			int key = controllerKeyMap.keyAt(i);
			int value = controllerKeyMap.get(key, -1);
			if (value != -1) {
				Log.d(TAG, "KEY=" + key + "  VALUE=" + value);
			}
		}
		Log.d(TAG, "---------------------------------------------");
	}
	
	private InputStream getInputStreamForFile(String fileName)
	{
		String woiAssetsPath = Cocos2dxHelper.getWOIAssetsPath();
		InputStream inputStream = null;
		
		try {
			if(woiAssetsPath.contains("obb")) {
				ZipResourceFile zipResourceFile = APKExpansionSupport.getAPKExpansionZipFile(startUpActivity, StartUpProject.getVersionCode(), 0);
				inputStream = zipResourceFile.getInputStream("assets/" + fileName);
			}
			else {
				inputStream = startUpActivity.getAssets().open(fileName);
			}
		}
		catch(IOException e) {
            e.printStackTrace();
            inputStream = null;
		}
		
		return inputStream;
	}
}
